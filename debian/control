Source: lgogdownloader
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Stephen Kitt <skitt@debian.org>
Section: web
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               libboost-date-time-dev,
               libboost-filesystem-dev,
               libboost-iostreams-dev,
               libboost-program-options-dev,
               libboost-regex-dev,
               libboost-system-dev,
               libcurl4-gnutls-dev,
               libjsoncpp-dev,
               librhash-dev,
               libtidy-dev,
               libtinyxml2-dev,
               ninja-build,
               pkgconf,
               qtwebengine5-dev [mips64el],
               qt6-webengine-dev [amd64 arm64 armhf i386],
               zlib1g-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/games-team/lgogdownloader
Vcs-Git: https://salsa.debian.org/games-team/lgogdownloader.git
Homepage: https://sites.google.com/site/gogdownloader/

Package: lgogdownloader
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: downloader for GOG.com files
 lgogdownloader is a client for the GOG.com download API, allowing
 simple downloads and updates of games and other files from GOG.com.
 .
 This package is only useful if you own games on GOG.com. There are a
 few free-as-in-beer games available for Linux, but the DFSG-free
 games available on GOG.com are not provided for Linux and are
 available in Debian anyway (lure-of-the-temptress,
 beneath-a-steel-sky, flight-of-the-amazon-queen).
